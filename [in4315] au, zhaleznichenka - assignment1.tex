\documentclass[a4paper]{article}

\makeatletter
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\normalsize\bfseries}}
\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\normalsize\bfseries}}
\makeatother

\usepackage[cm]{fullpage}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage[font=small,labelfont=bf]{caption}
\graphicspath{{./UML/}}
\pagenumbering{arabic}
\begin{document}

\begin{titlepage}

\begin{center}

Delft University of Technology\\
Faculty of Electrical Engineering, Mathematics and Computer Science\\[3cm]
\huge \bf{Car Online Auctioneering Platform} \\
Software Architecture Description\\[17cm]
\end{center}

\large \noindent 
Group 15: \\
Sietse Au (1382756) and Zmitser Zhaleznichenka (4134575)\\
\{S.T.Au, D.V. Zhaleznichenka\}@student.tudelft.nl\\

\noindent version 1.0\\
\today

\end{titlepage}

\setcounter{tocdepth}{4}
\setcounter{secnumdepth}{4}
\tableofcontents

\section{Introduction}

Behind most of the non-trivial online services there are large and complicated software systems that let these services to deliver the required functionality and satisfy such important non-functional requirements as response time, scalability, fault tolerance, security and more. To build the competitive systems that can successfully cope with the complexity, usual for the web applications nowadays, and constantly evolve to address the changes to the business rules without quality degradation, it is important to make right design decisions and properly document them during the whole software lifecycle starting from the very beginning.

This document provides an architecture description for a global car online auctioneering platform (COAP) and is based on the approach used in the 2nd edition of \emph{Software Systems Architecture: Working With Stakeholders Using Viewpoints and Perspectives} book by N. Rozanski and E. Woods (Addison-Wesley, 2012). The paper was written as an assignment for IN4315 Software Architecture course at TU Delft, The Netherlands and may be useful to junior software architects and students of engineering specialities who are interested in designing software architectures using viewpoints and perspectives. The paper describes a non-existing software system and the authors do not plan to develop it further and use in business. The provided architecture is not full and is limited by the core viewpoints specification.

COAP is a global trading platform that allows people all over the world to buy and sell their vehicles, the most of which are used. The main goal of a system is to provide potential buyers and sellers with information about the existing offers at the used vehicles market in their region, let them discuss the offers, bid on available vehicles and conclude the deals. The main features of COAP are the flexible and powerful search engine, easy-to-use and intuitive user interface, mobile application allowing to interact with available offers and place new ones. The system is monetized by letting users place paid premium offers, displaying ads and granting access to system APIs to partner sites.

The rest of this paper is organized as follows. In Chapter 2 we describe the context of a system, which includes the presentation of the main stakeholders and requirements overview. Also, in this chapter we devise and present different functional and not-functional scenarios that were used during the architectural design. Chapter 3 contains the description of various architectural views of a system, including context, functional, concurrency and deployment viewpoints.

\section{System Context}

\subsection{System Environment}
\label{sec:system-environment}
The discussed system is a distributed web application that is to be written from scratch. A number of third-party libraries will be actively used in its source code, but they should not be considered as the indispensable parts of COAP. The interaction with external software systems is limited by the following cases. First of all, the system should connect to a global payment operator to be able to accept payments from the customers. Secondly, the system should provide a set of trading APIs to the partner sites and use their APIs to integrate their data with the system databases, thus providing the customers with the wider options in their searches. Also, the system should be able to interact with Google Adsense service to display the ads. However, this service is considered to be only a supplement for the internal ads system and it is planned to use it only at the pages not covered with the advertisings provided by internal system. Usage statistics is planned to be gathered with Google Analytics which is the final external dependency. 

In addition to the named external dependencies, COAP should communicate with the Account Management System (AMS) and the Backup Site (BS). These internal systems should be developed within the client company but should not be the parts of COAP itself. This decision was made to simplify reuse of both systems in the forthcoming company products. AMS should gather and provide information about the customers, BS should be able to automatically create, store and restore backups of both COAP and AMS. 

While not being strictly dependent from third-party software components, the modular and distributed character of a system sets certain requirements to the internal communication between COAP components. It is planned to use a number of dedicated servers in several data centers around the globe to minimize latencies for all the users. The components deployed at the distinct sites should be able to communicate transparently for the user.

\subsubsection{Stakeholders}

The most important stakeholders whose concerns were considered while working on the document are

\begin{itemize}
\item {\bf Acquirers} --- Owners/senior managers of a client company. Make business-related decisions and develop the project strategy, together with architects work on system requirements, accept delivered product, analyze reports about system use.
\item {\bf Developers/Maintainers} --- create source code, layout and graphics of the system, constantly improve them during the project lifecycle, make technical decisions, suggest new features to the acquirers and implement them after approval. 
\item {\bf Architects} --- make architectural decisions and document them, check whether the system is being developed with concordance to the architecture, interact with both acquirers and developers to gather and analyze the requirements, update the architecture to reflect them. Notify developers about architecture changes. Suggest new features to the acquirers.
\item {\bf Testers} --- write and execute functional and non-functional tests to prove correctness of system operations. Sign off the releases.
\item{\bf System administrators/Production engineers} --- install and maintain development tools to provide developers and testers with a stable and comfortable environment, configure and maintain production and testing hardware, establish 24x7 control of the production servers.
\item{\bf Users} --- interact with the system by placing and reviewing car offers, provide feedback.
\item{\bf Support staff} --- interact with users to improve their experience with the system, gather feedback and report it to the acquirers.
\end{itemize}

\subsubsection{Overview of requirements}

\renewcommand{\labelenumi}{R\theenumi.}

\paragraph{Functional requirements}


The features provided by the COAP are bounded by the following list of functional requirements.

\begin{enumerate}
\item COAP is connected to the Account Management System that allows users to register and manage personal information.
\item An auctioning system allows users to bid on and auction cars. When auctioning a car, a user can provide a manual list of properties.
\item Offers management module provides a customer an overview of the offers he is interested in. This includes the offers placed by this customer, the offers he bod on, wish lists and purchase history. The module allows user to communicate with the other parties participating in a deal.
\item Search module should be developed to let users search through the available car offers given several pre-defined filters: model (one or more), vendor (one or more), class, year (one or more), distance from user (range), vehicle mileage (range), transmission (manual or automatic), type of fuel (one or more), power (range), price (range), color (range), country (one or more) and full-text search through the whole database.
\item Trending analysis should provide a dynamic overview of sales trends given filtering criteria such as in the Search module. This allows users to observe several trends such as drops in car prices, popular cars given a price range and so on, combinations are possible.
\item Payment system should support a step by step procedure to allow the user to pay for winning bids. In this procedure, the user will be validated and payment options can be chosen and finally a payment can be made by the user with direct feedback to the COAP. Also, the payment system is responsible for the micropayments for the premium access to the system.
\item Rating system lets the users rate the counterparty for each sale. The rating displayed should be viewable by a user given date range, this way the user can decide by himself whether another is trustworthy or not. 
\item COAP can perform automatic backups to the Backup Site and is able to recover its state from the backups. The backup schedule should be easily adjustable.
\end{enumerate}

\paragraph{Quality properties}

\subparagraph{Security requirements}

\begin{enumerate}[resume]
\item System databases contain the most valuable assets of the system, viz. customer data (including information about credit cards) and existing and legacy vehicles offers. Under no circumstances these data should leak from the company servers. 
\item DDoS attacks from the competitors can break the system operations. To avoid that, the system should have protection from the reasonably strong attacks of such kind. In case of a very powerful attack the performance of system should degrade gracefully, system engineers should be able to disable only the affected parts of a system while the rest of the services should remain operational.
\item Web interface of a system should successfully resist traditional web threats, i.e. remote code executions, SQL injections, format string and XSS vulnerabilities. Besides taking all usual counter-measures in the source code of the system itself, all third-party software used within the system should be constantly updated and properly configured to provide best protection against such threats. Any user misbehavior with potential threat to the system operations should be logged and reported.
\end{enumerate}

\subparagraph{Scalability requirements}

\begin{enumerate}[resume]
\item The largest automotive site in the Internet, eBay motors, reports on around 12 million unique visitors every month (2009) with each visitor viewing around 50 pages on average (2007). This means that if our system becomes a successful global automotive site it should scale to support up to 20 million unique visitors and 1 billion page views per month. If COAP will be enriched with the functionality other that automotive auctioneering, it should scale to support even more users. 
\item As it is very expensive to support an extremely large web system supporting millions of users and our system is assumed to be moderately popular during the initial stages after its deployment, it should be able to run at small number of commodity servers and scale on demand upon gaining popularity.
\end{enumerate}

\subparagraph{Availability requirements}

\begin{enumerate}[resume]
\item The system should aim for 99.99\% of availability of every its component to every user.
\item Occasional unavailability of certain system modules should not affect the availability of other modules.
\item In case of a system module fault at the certain site it should be possible to replace this module with the same module from the backup site in automatic mode. In this situation the performance degradation is possible, but it should stay within the performance limits allowed for operations in extreme situations. 
\item The respective notifications should be immediately sent to the network engineers on duty in any tracked faulty or suspicious situations.
\end{enumerate}

\renewcommand{\labelenumi}{\theenumi.}

\subsection{System Scenarios}

\subsubsection{Functional scenarios}

\subparagraph{User registration}

\emph{Overview:} A new user registers in a system.

\noindent\emph{System state:} None.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} The user fills in his personal information such as username, password, e-mail, name, gender, age and address and clicks on the submit button on the registration page.

\noindent\emph{Required system response:} The system verifies whether the username is unique, email address is valid, passwords match and the user is of appropriate age. If the validation succeeds, a user receives on-screen and email confirmation about the registration in the system, otherwise he sees an error on the registration screen.

\subparagraph{User login}

\emph{Overview:} User logs in.

\noindent\emph{System state:} None.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} The user fills in his username and password and presses the login button.

\noindent\emph{Required system response:} The system checks if the username and password tuple exist. In this case the user gets authenticated and receives an on-screen confirmation of the success of the action, otherwise the user receives a message that the username and/or password does not exist.

\subparagraph{User alters account information }

\emph{Overview:} The system processes a change in user account information.

\noindent\emph{System state:} The user is logged in.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} The user alters his account information and presses the update button.

\noindent\emph{Required system response:} The system validates the input. If the altered data is valid, the account information will be updated, otherwise the user will be notified
why an update didn't occur.

\subparagraph{User searches for cars on auction}

\emph{Overview:} System processes a user search given a number of criteria.

\noindent\emph{System state:} None.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} The user clicks on the search button in the user interface after activating some or none of the filters.

\noindent\emph{Required system response:} The system returns cars on auction matching the filters.

\subparagraph{User selects an auction}

\emph{Overview:} User selects an auction by using a URI.

\noindent\emph{System state:} Selected auction exists.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} User accesses the details of a given auction through his browser.

\noindent\emph{Required system response:} The system shows the selected auction.
\subparagraph{User places a bid on an auction}

\emph{Overview:} How the system processes a bid on an auction. 

\noindent\emph{System state:} User is logged in, the auction exists, and the deadline for it didn't pass.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} The user clicks on the bid button in the user interface after filling in an amount in a certain currency in the appropriate input field.

\noindent\emph{Required system response:} The user's bid becomes the auction's highest bid.

\subparagraph{Auction meets its deadline}

\emph{Overview:} System processes an auction which has met its deadline.

\noindent\emph{System state:} The auction exists and has ended.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} None.

\noindent\emph{Required system response:} The highest bidder and auctioneer are notified with a message containing the steps following an auction.

\subparagraph{Buyer pays for a winning bid}

\emph{Overview:} User pays for a winning bid after being notified.

\noindent\emph{System state:} User is logged in, is the highest bidder and has been notified by the system.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} None.

\noindent\emph{Required system response:} The buyer's payment gets validated and if valid, the buyer received a confirmation and otherwise a notification that the payment has been rejected.
When the payment has been confirmed, the seller receives a notification that the auction item has been paid.

\subparagraph{Seller marks sold item as sent}

\emph{Overview:} System marks an item in the seller's sales-list as sent.

\noindent\emph{System state:} Seller is logged in and is the seller of the auctioned and paid item.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} User marks an item as paid.

\noindent\emph{Required system response:} System shows the user that item has been marked. 

\subparagraph{Buyer rates seller}

\emph{Overview:} System processing the rating of a seller.

\noindent\emph{System state:} Buyer is logged in, and has bought an item, and the bought item has been marked as sent.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} Buyer fills in a rating form and clicks on the 'Submit'-button.

\noindent\emph{Required system response:} If not all of the required fields have been filled in, the system
notifies the user and shows the same form again with fields filled in which had been filled in before. If the submitted form is valid then the system processes 
the form.

\subparagraph{Trending analysis module updates statistics}

\emph{Overview:} Trending analysis updates statistics upon change in the system by user input or in-system-events.

\noindent\emph{System state:} None.

\noindent\emph{System environment:} The deployment environment is operating normally.

\noindent\emph{External stimulus:} A user has performed an action which triggers the trending analysis module or an in-system-event occurred which triggers the trending analysis module.

\noindent\emph{Required system response:} The system correctly processes the newly gained statistics.

\subsubsection{System quality scenarios}

\paragraph{Security scenarios}

\subparagraph{DDoS attack of a moderate size is detected}

\emph{Overview:} How the system behaves if it is faced with a DDoS attack that consumes all the bandwidth of communication channels and CPU time of the web servers.

\noindent\emph{System environment:} The deployed environment is working correctly.

\noindent\emph{Environment changes:} The system starts to receive around 500k TCP SYN flood packets per second. Web servers cannot handle such large amount of open connections and stop replying to the requests from the users.

\noindent\emph{Required system behavior:} The system detects a failure and reports to production engineers on duty. The engineers enable DDoS protection from a third-party company and update A-records from the affected parts of a system to point to the filtering systems that return cleared traffic.

\subparagraph{An attempt to use potential web vulnerabilities}

\emph{Overview:} How the system behaves if malicious user tries to exploit such vulnerabilities as SQL injections or format string attacks.

\noindent\emph{System environment:} The deployed environment is working correctly.

\noindent\emph{Environment changes:} A user tries to exploit SQL injection by entering strings with special characters into the input fields while creating an offer to sell a vehicle.

\noindent\emph{Required system behavior:} The raw data is correctly escaped and processed, no failure occurs. A system detects an attempt to use SQL injection and writes a report to the logs.

\paragraph{Scalability scenarios}

\subparagraph{Server load increases fastly}

\emph{Overview:} How the system behaves in case of spontaneous interest to it that leads to a significant increase of connections.

\noindent\emph{System environment:} The deployed environment is working correctly. CPU utilization of the used servers is around 60\%.

\noindent\emph{Environment changes:} After the introduction of a new feature that was covered in an issue of a popular magazine and redistributed in the blogs, the system faces three times more visitors than on average. CPU utilization jumps to 100\%, latency increases, new visitors continue to come.

\noindent\emph{Required system behavior:} System engineers are able to quickly deploy the new instances of the system modules at Amazon EC2 facilities, wire them with the existing infrastructure and decrease the load. Upon purchase of  new hardware, these instances are deployed there.  

\paragraph{Availability scenarios}

\subparagraph{Updating system modules while keeping the service available}

\emph{Overview:} How the system deals with a need to update its modules to introduce new features and fix bugs.

\noindent\emph{System environment:} The deployed environment is working correctly.

\noindent\emph{Environment changes:} There is a need to update the files of a web application responsible for the search in the vehicles database.

\noindent\emph{Required system behavior:} As the application is deployed at several physical servers, production engineers disable them one by one, deploy new version of an application and enable again. While a certain server is unavailable because of being updated, the load balancer routes the traffic to the remaining servers which have sufficient capacity to cope with the incoming requests.

\subparagraph{Hardware failure at the database server}

\emph{Overview:} How the system responds to the hardware failures.

\noindent\emph{System environment:} The deployed environment is working correctly.

\noindent\emph{Environment changes:} Due to the problems with the hard drive a master database server covering write operations while adding new vehicles for US region stops responding to the requests.

\noindent\emph{Required system behavior:} Failure in system operation is detected automatically. An urgent report is sent to the team of production engineers on duty. One of the slave instances of databases changes the master. An additional slave instance of a database from the backup site is added to the production servers infrastructure.

\pagebreak

\section{Architectural Views}
\subsection{Context Viewpoint}

\subsubsection{Context model}

You can find the context model at Figure~\ref{fig:context-model}. This model represents the system environment and relates COAP with the internal components not being parts of the system itself and with the external entities. The explanations on the interaction with the external services and internal subsystems can be found in Section \ref{sec:system-environment}.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.9\textwidth]{Context_model.png}
\caption{COAP context model.}
\label{fig:context-model}
\end{center}
\end{figure}

\subsection{Functional Viewpoint}

You can find the functional model of a system at Figure~\ref{fig:functional-model}.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.9\textwidth]{Functional_Viewpoint.png}
\caption{COAP functional model.}
\label{fig:functional-model}
\end{center}
\end{figure}

\subsubsection{Functional elements}
\label{subsubsec:functional-elements}

This section gives a short description for each functional element presented at the Figure~\ref{fig:functional-model}.

\begin{itemize}
\item[] \textbf{Auctions} --- the element is responsible for processing requests by users who want to view or add auctions. It also has an interface to allow bidding on certain auctions.

\item[] \textbf{Ratings} --- the element is responsible for processing user ratings.

\item[] \textbf{SalesProcessor} --- the element is responsible for processing sales records and providing a user an interface to manage his sales.

\item[] \textbf{Bids} --- the element is responsible for processing bids done by each user. It uses an interface to access details about the auction to function correctly.

\item[] \textbf{TrendingAnalysis} --- the element is responsible for processing statistics provided by the Auctions, Rating and SalesProcessor elements.

\item[] \textbf{Accounts} --- the element is responsible for authentication, authorization and account management for the users.

\item[] \textbf{Interface} --- web and mobile interfaces presenting the functionality of the application to the customers.

\end{itemize}

The system is based on MVC paradigm with each functional element having its model and controller. The views are provided by Interface functional element. 

\subsection{Concurrency Viewpoint}

As follows from the system specification, COAP should not face any serious concurrency issues that must be addressed at the architecture description layer. COAP is a distributed web application with many separate applications covering the respective parts of functionality and interacting with each other by means of the web service requests. Usually, when one application requests some data from another one, it issues a web service request that is executed after creation of the new instances of required objects inside the target application. After responding to the request, the created objects are getting disposed. The interaction with the database is organized within the database connecting applications that provide the only way to access the data from it. Concurrency issues within the database are handled by the database management software.

The only problems in such setup are the load balancing and availability of sufficient server instances. While there can be concurrency issues related to the performance optimizations within certain application modules, they should be addresses at the development level and should address only those applications, not interactions between them.

\subsection{Deployment Viewpoint}

\subsubsection{Runtime platform model}

You can find the deployment model of a system at Figure~\ref{fig:deployment-model}.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.9\textwidth]{Deployment_diagram.png}
\caption{COAP deployment model.}
\label{fig:deployment-model}
\end{center}
\end{figure}

Hardware specification is the following.

\begin{enumerate}

\item \textbf{Client hardware}. Client nodes are not represented in the deployment model explicitly though it is assumed that HTTP and Web Services requests come to the load balancer from the clients. Any machine with a contemporary browser or a COAP mobile app and Internet connection may operate as the client node.

\item \textbf{Front-end server}. Front-end servers are hosting front-end application that receives requests from client browsers and responses with the web pages. Also, front-end servers are responsible from delivering static content (i.e. car photos) from a static storage. Servers communicate with the load balancer to execute web service requests to the application servers and with the Account Manager to handle user authorization issues. Use HP ProLiant DL300 hardware for web pages generation functionality and HP P2000 G3 MSA array system as a static storage. At least 20TB of static storage space is initially needed.

\item \textbf{Load balancer}. Is responsible for correct processing of extreme amount of user requests and their proper distribution to the web and application servers. Citrix MPX 21500 solution is used.

\item \textbf{Application server}. The application server instances control the operations of Java-based services, namely Sales Processor, Trending Analysis, Auctions, Bids, Account Manager and Ratings. Each of the servers may support any number of the enumerated services, and any of the services can be deployed at multiple nodes. The nodes do not need to communicate with each other, only with the load balancer and the respective databases. 

\item \textbf{Database server}. Database servers manage operations of user and auctions databases as well as a search index. Use HP ProLiant DL500 hardware. At most 2TB of storage is required for database operations. 

\end{enumerate}

\subsubsection{Software dependencies}

All the server software except for the load balancer should run FreeBSD 9.0 OS. Load balancer uses proprietary Citrix NetScaler software for its operations.

Web servers use JBoss AP 7 as an application container and Nginx 1.2.0 as a web server. Application servers use JBoss AP 7 application container to provide an environment for Java modules of COAP. Auctions and User databases are operated under MySQL 5.5 database engine. Oracle BIEE 11g is used as a data warehouse for auctions database indexing.

\subsubsection{Network model}

The most important requirement to the network model is the capacity of the outbound channel from the web server to the clients through the load balancer. To ensure correct and fast network operations, the outbound channel should have capacity of at least 10 Gbps. For inbound channel 1 Gbps capacity is sufficient. All the hardware links between internal components should have at least 1 Gbps capacity.

\end{document}