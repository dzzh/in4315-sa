\documentclass[a4paper]{article}

\makeatletter
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\normalsize\bfseries}}
\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\normalsize\bfseries}}
\makeatother

\usepackage[cm]{fullpage}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage[font=small,labelfont=bf]{caption}
\graphicspath{{./yEd/}}
\pagenumbering{arabic}
\begin{document}

\begin{titlepage}

\begin{center}

Delft University of Technology\\
Faculty of Electrical Engineering, Mathematics and Computer Science\\[3cm]
\huge \bf{Car Online Auctioneering Platform:} \\
Moving Towards Service-Oriented Architecture\\[17cm]
\end{center}

\large \noindent 
Group 15: \\
Sietse Au (1382756) and Zmitser Zhaleznichenka (4134575)\\
\{S.T.Au, D.V. Zhaleznichenka\}@student.tudelft.nl\\

\noindent version 1.0\\
\today

\end{titlepage}

\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{3}
\tableofcontents

\section{Introduction}

In the previous assignment we have provided an architecture description for a global car online auctioneering platform (COAP). While describing the architecture for this project, our design decisions were based on the assumption that we have to write the most part of the source code from scratch and use proprietary interfaces to manage interconnections within the internal parts of the system. Also, it can be said that while working on the previous assignment, we described component-based architecture. In this assignment we will move COAP towards process-enabled service-oriented architecture (SOA), will make the design decisions based on the existing SOA protocols and technologies and will switch from the proprietary internal services to the external ones wherever possible to let the virtual COAP owners to cut costs on the initial stage of development and deployment of the system. 

%TODO outline

\section{Services definition}

To make COAP more flexible and scalable we suggest to base it on the services provided by the Amazon Web Services cloud computing platform (AWS)\footnote{http://aws.amazon.com}. AWS contains a wide range of services that allow to run enterprise applications of arbitrary complexity and require much less efforts from the developers and system administrators to create and maintain the systems supporting business processes if comparing with keeping everything in own data centers. AWS allows its customers to outsource their computing facilities to a single provider thus minimizing the amount of work needed to combine company services into a single mechanism.

In this chapter we describe the distribution of COAP functionality into a set of web services and their organization into a process-enabled service-oriented architecture. 

\subsection{Infrastructure}

In the previous assignment we planned the architecture of the application for further deployment at client data centers. This solution implied the need to purchase and maintain a large amount of expensive hardware as well as to keep 24/7 engineering team to maintain the servers and resolve potential issues with their operations. The need to distribute the application through several data centers all over the world for ensuring small latencies and distributing the load would cause significant investments on the initial stage of the system lifecycle. While being cheaper than leasing computing capacities in a long-term period, the usage of an own data center may be unprofitable if using it merely for supporting a new application that operates in unstable markets. For this reason we suggest to use Amazon Elastic Compute Cloud (EC2) as the main computing platform for hosting both front-end and back-end processing nodes.

 To support database operations, we suggest switching from MySQL databases deployed at the client servers to Amazon Relational Database Service (RDS) and use Amazon ElastiCache caching solution. In the previous assignment it was assumed that the selection of the caching provider is done by developers and is application-oriented. The usage of ElastiCache is beneficial to such an approach as it allows to keep all the caches in one place and requires from the developers to learn only one API specification that they can later use to support any of the company products deployed at AWS.

In addition to this services, it is essential to enable Auto Scaling and Elastic Load Balancing services to manage the load of the computing platform. In the previous assignment it was suggested to purchase a hardware load balancer from Citrix to distribute the load between the application servers but there was no solution to significantly scale the applications in the limited amount of time. For this situation, temporary use of AWS processing nodes in addition to the ones in the client data centers was suggested. 

To store static content (i.e. vehicle images, submitted by the users), we suggest switching from an own storage platform using HP hardware and Nginx web server to Amazon Simple Storage Service (S3) which is a standard choice for many companies operating with large volumes of user data. This service allows to distribute any number of files throughout the web with no need from the client to plan storage capacities and available network bandwidth in advance to prevent denials of service in case of excessive load or hardware/network failures.

\subsection{Internal Services}

\paragraph{COAP Search}

The searching service is responsible for issuing queries to the search index and returning the ordered lists of vehicles that match user preferences. It should support an extensive set of filtering criteria allowing to query for fine-grained results. In the previous version of the architecture description we planned to build the search index based on the denormalized data warehouse managed with Oracle BIEE solution.

It is suggested to base the searching functionality on the Amazon CloudSearch engine, that provides a scalable indexing \& retrieval solution that can be used to manage the search queries of arbitrary frequency and complexity.

\paragraph{COAP Bidding}

Bidding is another essential logic-centric COAP service that is responsible for updating the auction states. The service should allow users to bid on the auctions they like and see the bidding history. The service should not allow two customers to make the same bids in the same time thus ensuring that each running auction at any time has at most one highest bidder. Also, the Bidding service has to update the state of the auctions upon reaching certain time, price or other conditions specified by the author of the auction. It should interact with the Communication service to let participating customers know the auction state was changed. Also, it should interact with the Payment service to transfer the responsibility to it when the auction is completed and the winner is determined.

\paragraph{COAP Payment}

The Payment service has two goals. First of all, it should enable money transfers between the users of the system as payments for the auctions. Secondly, it should accept the payments from the users to the COAP administration to grant them access to the premium features. 

The first part of the payment system handles money transfers between the different users and has thus be secure enough to address potential security issues and prevent the fraud. It should also be flexible enough to give the users maximum options in choosing the payment method. It is needed to remember that the environment of the system is the following.

\begin{enumerate}
\item Most users do not have a large history of successful transactions as they use the platform to trade only one car in several years.
\item Most money transfers are rather big, starting from a thousand euros in equivalent and higher.
\item Most deals are concluded by people who live nearby and have to meet at least once to assess the quality of the traded item and transfer the money in person. However, there are situations where the traded item can be sent far away without the personal assessment from the auction winner.
\end{enumerate}

Having this setup, the following should be supported by the Payment service. First of all, it should be possible to transfer the money via all popular online payment systems, like PayPal and MoneyBookers. Secondly, the system should allow participants to share bank account numbers to make direct payments and share other details to perform the payments the other way (i.e. hands-to-hands, via Western Union and so on). For this situations, an owner of an auction should be able to mark payment as completed and thus conclude the deal even if there was no money transfer via an online payment system. Thirdly, it should be possible for a winner to transfer the money to the COAP account using one of the online systems to make the deal more secure. In this scenario, when COAP receives money, it informs the auction owner about that and he can send the purchase to the winner. Upon receipt of the purchase and checking its condition, the winner notifies COAP and the Payment system transfers the money to the auction owner's account.

To support micro payments from the users for granting them access to certain premium features, COAP should be connected to one of the global payment systems that supports credit card payments, global and local online money transfer systems, i.e. GlobalCollect. Such connection should be established via the adapter service. 

\paragraph{COAP Trading API}

One of the ways to monetize COAP is to provide the partner web sites partial access to the COAP databases on a paid basis. As COAP is a large trading platform, the interaction with it may be useful for many third-party companies who can enrich the provided data about the vehicle offers and use it for analyses or enrich it and place at their own platforms. To interact with the external partners, COAP has to provide a set of APIs that grant clients access to certain pieces of COAP functionality, mainly the search, auction creation and view. Such APIs should be implemented as a facade service that is a single access point for all the clients. Trading API should not only grant or deny access to certain COAP services based on client credentials and serve as a proxy for data requests, but also gather the statistics of data usage and automatically apply access restriction rules to prevent illegal access to the COAP data in case of the expiration of the access period or in other situations.

\paragraph{COAP History}

History service should let the customers access the whole history of their COAP activities, including own auctions (both current and completed), bids at the existing auctions and purchases. Also, the History service should provide the basic information about the counter-parties, like their ratings and status. This service should provide the sellers possibility to change the information about their current auctions (i.e. add pictures or adjust the description).

\paragraph{COAP Communication}

As COAP deals with such expensive and complex auction lots as used vehicles, it is important to let trading parties to communicate about the placed auctions. The communication mechanism should be non-intrusive and secure. In COAP, all the communication with the administration and between the users should be managed within a separate Communication service. This service is responsible for the following.

\begin{enumerate}
\item Delivery of emails from the administration to the users participating in the auction about the changes in the auction state. 
\item Delivery of the emails from the administration to the users concerning the changes in their accounts (registration, password change request, etc.)
\item Delivery of mass emails to all the customers about the changes in the site policies, COAP news updates, etc.
\item Delivery of personal emails from one user to another concerning the auctions (clarifying questions, shipment updates, etc.) without disclosing the real emails of the users to the third parties.
\item Management of the comments underneath the auctions and notifying the auction owner about the new comments.
\item Subscription management to let users (un)subscribe from(to) certain types of personal and mass emails from the administration and other users.
\end{enumerate}

\paragraph{COAP Ratings}

Ratings service should have two related responsibilities. First, it allows users to evaluate their counter-parties after completing the auction. Secondly, it employs a number of data analysis techniques to prevent fraud from the malicious users. The service is related to the AMS and History services.

After completing of an auction, the service should let the participants to evaluate their counter-parties. It should provide two different evaluation forms for an owner and a winner that will contain the questions concerning most important issues within the auction, namely the correctness of description, delivery speed, payment accuracy and so on. All the questions should have a quantifiable measure. Based on these data, a single user rating value should be calculated which is the combination of ratings for the certain issues. Each user having both owned and won items should have two different ratings for these activities.

Fraud prevention module should automatically detect and respond on such undesirable user activities as spamming, advertising improper services, attempting not to pay for the lots and so on. It should be rule-based and allow to add and change fraud detection rules on the fly. The service should be able to respond to the fraud by logging fraudulent activities, blocking malicious users and so on.

\paragraph{Trends analysis}

Large web systems like COAP gain much data about the behavior of their users. This data can and must be analyzed to improve the satisfaction of the users and increase site popularity. Also, it can be provided to the third parties for their analyses. 

To support the analytical processing of COAP data, it is planned to build a trending analysis service that will provide with the diagrams of the current, recent and historical user activities. The service will be created as an adapter to the data warehouse built with Oracle BIEE that is planned to use for keeping historical data and will be used to create a number of interfaces in the administration panel.

\paragraph{Sites Communicator}

COAP is a distributed web application. Most of its services are deployed in a number of data centers all over the world and are meant to support the operations for the given region only. In most situations it works well as the bidders do want to check the traded car themselves and do not want to make payments to other countries and pay for the delivery. However, in some situations the customers want to see the offers from another part of the world, either to find some rare vehicle, or to find a cheap offer, or just to compare the prices. For this, they have to specify a different region in the search filters. 

To provide the customers data about the vehicles and their owners from different parts of the world, the parts of COAP that are deployed in different regions should communicate. This communication is meant to be done via the Sites Communicator which is the gateway between the deployment sites. The Sites communicator serves as a proxy between the COAP services and is routing the requests to the sites that contain data for the regions specified by customer.

\paragraph{User Interface}

All the services described above operate and interconnect with the data exchange protocols and do not have any user interfaces. All interaction with the users is contained in a number of separate interface services that provide the layouts, graphics and forms for the clients. 

In addition to the web interfaces that are the main customer entry point to the system, COAP should have a mobile application available for all popular mobile platforms that allows users both to place auctions and bid for them, so mostly copies the functionality of the web service but has all the benefits the mobile applications have in comparison with the web interfaces.

For both web and mobile interfaces to operate, they should rely on a single adapter service that links all COAP services into one mechanism and provides a number of a high-level APIs accept and return XML-based data that is later processed by the interfaces. For better flexibility, the interfaces are also able to communicate directly with the back-end parts of COAP though this communication should be avoided as much as possible. 

Having built an adapter service providing the access to the core services from the interfaces, it is easy to adjust the COAP views and build the new interfaces for the other devices or regions. The usage of an adapter services simplifies the maintenance of the core services as the most potential changes in their APIs will only lead to the changes in the source code of the adapter and will not require to rewrite the code of many different interface services built for the different means.

\subsection{External Services}

\paragraph{Account Management Service}

As we did it earlier, we claim that the account management system (AMS) should be separated from the main project functionality for flexibility and security matters. In the service-oriented COAP the AMS should thus be an internal data-centric web service that is connected to its own database and is used to handle authentication and authorization issues. AMS covers such functionality as registration of new customers, management of their details and roles within the COAP. Also, the AMS is responsible for assigning each client an unique id that can be used throughout the systems exploiting AMS for referencing the respective user.

\paragraph{Backup Site}

It is always important to back up the state of the web applications, especially if they are simultaneously used by many people. Sometimes it is needed to restore a database or certain data files due to the hardware corruption, sometimes backups are used to easily deploy the working system to additional hardware instances, sometimes the archived versions of files are useful in the retrospective analyses. 

Application deployment in the cloud environment can mitigate most availability issues that can arise due to the hardware faults but cannot protect from the human errors and software bugs leading to data losses. Thus, careful backups management is still of a primary importance in cloud setups. To handle backups issues, we need to develop a backup management service called Backup Site (BS) that has to be separated from the core COAP functionality and will provide COAP (and potentially other applications) its APIs to perform backups and restore systems from them. The BS is an internal data-centric web service that uses Amazon S3 as the data storage. Those client applications that want to use BS functionality, should work not with the BS itself but rather with a functionality-adding service that is calling BS APIs for setting up and adjusting backup schedules and is called by the BS during the backup routines.

\paragraph{Google Interface Services}

To enrich the functionality of COAP web interfaces, we suggest to use Google Adsense, Google Analytics and Google Maps services in there. Adsense will be used to display users the advertisements, Analytics will be used to gather the user statistics and Maps will be used to show the users the location of the auction owners so they can plan their routes to see the traded cars.

\paragraph{GlobalCollect payment system}

Global Collect\footnote{http://globalcollect.com} is planned to use as an external payment provider that can accept micro payments from the users to the administration for the premium features of the services. Also, it can be used to accept the money sent to COAP as a trusted third party during the auction. Global Collect will be connected with the internal payment system via an adapter.

\subsection{Layered Dependency Diagram}

You can find the diagram at Figure~\ref{fig:dld}.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.5\textwidth]{DLD.png}
\caption{COAP Dependency Layered Diagram.}
\label{fig:dld}
\end{center}
\end{figure}

\section{Business Process}
The business process described in this section is the trading process. In the diagram at Figure~\ref{fig:BPD} you see how the process orchestrates the other services to facilitate the car trading. Note that service-specific cases such as not being able to login and other service-specific details are not included in the diagram to keep the scope of the diagram limited to the trading process.

The diagram contains a user (Bidder) who wants to buy a car and another user (Seller) who wants to sell a car. To simplify things it is assumed that buyer in the diagram selects the car of the seller in the diagram and that the seller has put the car in the auction before the buyer has entered a search query. 

\begin{figure}[hbtp]
\begin{center}
\includegraphics[width=0.5\textwidth]{BPMN/tradingprocessservice.png}
\caption{The BPD of the Trading Process.}
\label{fig:BPD}
\end{center}
\end{figure}

\end{document}